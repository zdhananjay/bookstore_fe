import axios from "axios";
import React, { useEffect, useState } from "react";
const baseURL = "http://localhost:8080"

function Book() {

    let [books, setBooks] = useState([]);
    let [catid, setCatId] = useState("");

    useEffect(() => {
        axios
            .get(`${baseURL}/books`)
            .then((response) => { setBooks(response.data) })
            .catch(error => { console.log(error.data) });
    },[]);

    var onSubmit = (category_id) => {
        setCatId(category_id);
        console.log(catid);
        axios
        .get(`${baseURL}/books/${catid}`)
        .then((response) => { setBooks(response.data) })
        .catch(error => { console.log(error.data) });
    };


    return (
        <div className="App container margin-top" >
            <table className="table table-responsive-md table-bordered w-80%">
                <thead>
                    <tr>
                        <th>BookId</th>
                        <th>BookName</th>
                        <th>Author</th>
                        <th>Price</th>
                        <th>ISBN</th>
                        <th>Category</th>
                    </tr>
                </thead>
                <tbody>
                {books.map((book) => {
                    return (
                        
                            <tr key={book.book_id}>
                                <td>{book.book_id}</td>
                                <td>{book.book_name}</td>
                                <td>{book.author}</td>
                                <td>{book.price}</td>
                                <td>{book.isbn}</td>
                                <td><button className="login-btn" type="button" 
                                                onClick={()=> onSubmit(book.bookCategory.category_id)}>
                                                    {book.bookCategory.category_title}</button></td>
                            </tr>
                        
                    )
                })}
                </tbody>
            </table>
        </div>
    );
}

export default Book; 