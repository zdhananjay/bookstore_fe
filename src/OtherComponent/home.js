import axios from "axios";
import React, { useEffect, useState } from "react";
const baseURL = "http://localhost:8080"

function Home() {

    let [books, setBooks] = useState([]);

    useEffect(() => {
        axios
            .get(`${baseURL}/books`)
            .then((response) => { setBooks(response.data) })
            .catch(error => { console.log(error.data) });
    },[]);


    return (
        <div className="App container margin-top" >
            <table className="table table-responsive-md table-bordered w-80%">
                <thead>
                    <tr>
                        <th>BookName</th>
                        <th>Author</th>
                        <th>Price</th>
                        <th>ISBN</th>
                        <th>Category</th>
                    </tr>
                </thead>
                {books.map((book, book_name) => {
                    return (
                        <tbody>
                            <tr key={book_name}>
                                <td>{book.book_name}</td>
                                <td>{book.author}</td>
                                <td>{book.price}</td>
                                <td>{book.isbn}</td>
                                <td>{book.bookCategory.category_title}</td>
                            </tr>
                        </tbody>
                    )
                })}

            </table>
        </div>
    );
}

export default Home; 