import React, { useEffect, useState } from "react";
import './index.css'
import 'bootstrap/dist/css/bootstrap.css'
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
const baseURL = "http://localhost:8080"


function Login() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const navigate = useNavigate();

    

    const handleEmail = (e) => {
        setEmail(e.target.value);
    }
    const handlePass = (e) => {
        setPassword(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(email, password)

        axios.post(`${baseURL}/user/login`, {
            email: email, password: password
        })
            .then((response) => {
                console.log(response.data)
                const role = response.data.userRole;
                const username = response.data.firstName;

                localStorage.setItem("role",role);
                localStorage.setItem("username", username)

                if(role === "CUSTOMER")
                { navigate("/customerDashboard") }
            })
            .catch((error) => console.log(error.data))

        
        }

    return (
        <div className="page">
            <div className="cover">
                <h1>Login</h1>

                <label htmlFor="email">Email</label>
                <input className="input" type={email} placeholder="example@email.com" name="email"
                    onChange={handleEmail} value={email} />
                <label htmlFor="password">Password</label>
                <input className="input" type={password} placeholder="*****" name="password"
                    onChange={handlePass} value={password} />
                <button className="login-btn" type="submit" onClick={handleSubmit}>Sign In</button>
                <div><h6>Not yet user?</h6>
                    <Link to={'/register'} className="btn " >Register</Link>
                </div>
            </div>
        </div>
    );
}

export default Login;