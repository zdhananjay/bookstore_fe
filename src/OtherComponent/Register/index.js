import React, { useEffect, useState } from "react";
import './index.css'
import 'bootstrap/dist/css/bootstrap.css'
import axios from "axios";
import { Link } from "react-router-dom";
const baseURL = "http://localhost:8080"


function Register() {

    const [user, setUser] = useState({firstName: '',lastName: '',email: '',password: '',userRole:'0'});
        
    const handleChange = (e) => {
        const updatedUser = {...user};
        updatedUser[e.target.name]= e.target.value;  
        setUser(updatedUser);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        axios.post(`${baseURL}/user/register`, user)
            .then((response) => {
                console.log(response.data)
            })
            .catch((error) => console.log(error.data))
        console.log(user.firstName,user.lastName,user.email, user.password)
    }

    return (
        <div className="page">
            <div className="cover">
                <h1>Login</h1>
                <label htmlFor="Text">First Name</label>
                <input className="input" type={"text"} placeholder="firstname" name='firstName'
                    onChange={handleChange} value={user.firstName} />
                    <label htmlFor="Text">Last Name</label>
                <input className="input" type={"text"} placeholder="lastname" name="lastName"
                    onChange={handleChange} value={user.lastName} />
                    <label htmlFor="email">Email</label>
                <input className="input" type={"email"} placeholder="example@email.com" name="email"
                    onChange={handleChange} value={user.email} />
                <label htmlFor="password">Password</label>
                <input className="input" type={"password"} placeholder="*****" name="password"
                    onChange={handleChange} value={user.password} />
                <button className="login-btn" type="submit" onClick={handleSubmit}>Register</button>
                <div><h6>Already user?</h6>
                <Link to={'/login'} className="btn "  >Login</Link>
                </div>
            </div>
        </div>
    );
}

export default Register;