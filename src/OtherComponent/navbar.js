import React from "react";
import { Link } from "react-router-dom";
import './index.css'


function Navbar() {
  return (
    <div>
      <nav className=" nav navbar navbar-expand-lg navbar-light  mb-5">
        <Link to={`/home`}  className="navbar-brand "><h1>BookStore</h1></Link>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <Link to={'/home'}  className="nav-link">
                <h3>Home</h3>
              </Link>
            </li>
            <li className="nav-item">
              <Link to={'/home'}  className="nav-link">
                <h3>Features</h3>
              </Link>
            </li>
            <li className="nav-item">
              <Link to={'/home'} className="nav-link">
                <h3>Pricing</h3>
              </Link>
            </li>
            <Link to={'/login'} className="nav-link nav-right" ><h3>Login</h3>
              </Link>
          </ul>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;