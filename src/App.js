
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import Navbar from './OtherComponent/navbar';
import Home from './OtherComponent/home';
import { Route, Routes } from 'react-router-dom';
import Login from './OtherComponent/Login';
import Register from './OtherComponent/Register';
import Book from './Book';

function App() {
  return (

    <>
    <Navbar/>
      <Routes >
        <Route path='*' element={<Home /> }></Route>
        <Route path="/books" element={<Home />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/register" element={<Register />}></Route>
        <Route path="/customerDashboard" element={<Book />}></Route>
      </Routes>
    </>



  );
}

export default App;
